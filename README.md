st - simple terminal
--------------------
st is a simple terminal emulator for X which sucks less.

This fork contains the following patches:
- Anysize
  - Allows st to resize to any pixel size
- Boxdraw
  - Custom rendering of lines/blocks/braille characters for gapless alignment
- ExternalPipe
  - Reading and writing st's screen through a pipe
- Font2
  - Allows you to add a spare font to go along with the default
- Netwmicon
  - Enables to set _NET_WM_ICON which hardcodes an icon for st
- ScrollBack
  - Scroll back through terminal output.
- VertCenter
  - Vertically center lines in the space available if you have set a larger chscale in config.h

Some elements of this fork such as `st-copyout` are taken from [Luke Smith](https://lukesmith.xyz). and [another patch](https://github.com/Dreomite/st/commit/e3b821dcb3511d60341dec35ee05a4a0abfef7f2) was applied to fix the glyph issues.


Requirements
------------
In order to build st you need the Xlib header files.


Installation
------------
Edit config.mk to match your local setup (st is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install st (if
necessary as root):

    make clean install


Running st
----------
If you did not install st with make clean install, you must compile
the st terminfo entry with the following command:

    tic -sx st.info

See the man page for additional details.

Credits
-------
Based on Aurélien APTEL <aurelien dot aptel at gmail dot com> bt source code.

